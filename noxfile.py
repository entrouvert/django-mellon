import shlex
import sys
from pathlib import Path

import nox

nox.options.reuse_venv = True


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)  # pylint: disable=exec-used
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def get_lasso3(session):
    src_dir = Path('/usr/lib/python3/dist-packages/')
    venv_dir = Path(session.virtualenv.location)
    for dst_dir in venv_dir.glob('lib/**/site-packages'):
        files_to_link = [src_dir / 'lasso.py'] + list(src_dir.glob('_lasso.cpython-*.so'))

        for src_file in files_to_link:
            dst_file = dst_dir / src_file.name
            if dst_file.exists():
                dst_file.unlink()
            session.log('%s => %s', dst_file, src_file)
            dst_file.symlink_to(src_file)


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        f'django{django_version}',
        'psycopg2-binary',
        'WebTest',
        'django-webtest',
        'httmock',
        'pytest',
        'responses',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)
    get_lasso3(session)


@nox.session(tags=['jenkins'])
@nox.parametrize('django', ['>=4.2,<4.3'])
def tests(session, django):
    setup_venv(
        session,
        'pytest-cov',
        'pytest-random',
        'pytest-django',
        'pytest-freezer',
        'mock<4',
        'pyquery',
        'lxml',
        'pytz',
        django_version=django,
    )

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov=mellon',
            '--cov-config',
            '.coveragerc',
            '-v',
            f'--junitxml=junit-coverage.django-{django}.xml',
        ]

    if not session.interactive:
        args += ['-v']

    args += session.posargs + ['tests/']

    session.run(
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'tests.settings',
        },
    )


@nox.session(tags=['jenkins'])
def pylint(session):
    setup_venv(session, 'pylint<3', 'astroid<3', 'pylint-django', 'nox')

    for file in [Path('/var/lib/jenkins/pylint.django.rc'), Path(__name__).parent / 'pylint.django.rc']:
        if file.exists():
            break
    else:
        print('No pylint RC found')
        sys.exit(0)

    pylint_command = [
        'pylint',
        '-f',
        'parseable',
        '--load-plugins',
        'pylint_django',
        '--django-settings-module',
        'tests.settings',
        '--rcfile',
        str(file),
    ]

    if not session.posargs:
        pylint_command += ['mellon', 'noxfile.py']
    else:
        pylint_command += session.posargs

    if not session.interactive:
        session.run(
            'bash',
            '-c',
            f'{shlex.join(pylint_command)} | tee pylint.out ; test $PIPESTATUS -eq 0',
            external=True,
        )
    else:
        session.run(*pylint_command)


@nox.session(tags=['jenkins'])
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def django_admin(session):
    setup_venv(session)
    get_lasso3(session)

    django_admin_command = ['django-admin']
    if not session.posargs:
        django_admin_command += ['--help']
    else:
        django_admin_command += session.posargs

    session.run(*django_admin_command)


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
