# Generated by Django 2.2.26 on 2022-10-03 10:09

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mellon', '0005_drop_rename_issuer'),
    ]

    operations = [
        migrations.AddField(
            model_name='usersamlidentifier',
            name='nid_format',
            field=models.TextField(null=True, verbose_name='NameID Format'),
        ),
        migrations.AddField(
            model_name='usersamlidentifier',
            name='nid_name_qualifier',
            field=models.TextField(null=True, verbose_name='NameID NameQualifier'),
        ),
        migrations.AddField(
            model_name='usersamlidentifier',
            name='nid_sp_name_qualifier',
            field=models.TextField(null=True, verbose_name='NameID SPNameQualifier'),
        ),
        migrations.AddField(
            model_name='usersamlidentifier',
            name='nid_sp_provided_id',
            field=models.TextField(null=True, verbose_name='SAML NameID SPPRovidedID'),
        ),
    ]
