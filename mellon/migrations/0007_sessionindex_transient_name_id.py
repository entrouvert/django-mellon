# Generated by Django 2.2.26 on 2022-10-03 15:23

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mellon', '0006_nameid_attributes'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionindex',
            name='transient_name_id',
            field=models.TextField(null=True, verbose_name='Transient NameID'),
        ),
    ]
